/*
 * main.c
 *
 *  Created on: 1 lis 2019
 *      Author: kamil
 */
#define _GNU_SOURCE 1
#include <main.h>
#include <stdio.h>
#include <array_size.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <llog.h>
#include <limits.h>
#include <stdbool.h>
#include <fcntl.h>
#include <assert.h>
#include <usbd_def.h>
#include <errno.h>
#include <usb_device.h>
#include <ctype.h>

void wdg_refresh(void) {
	// HAL_IWDG_Refresh(&hiwdg);
}

void abort(void) {
	asm("bkpt #0");
	for(;;);
}

extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;
#define getline __getline

// from stdio.c
extern void stdin_ioctl_set_blocking(bool state);

struct main_s {
	uint32_t adcdma[1024 * 3];
	unsigned count;
	volatile uint64_t sum1;
	volatile uint64_t sum2;
	volatile bool sumready;
};

struct main_s m;


static inline
void init() {
	HAL_Init();
	SystemClock_Config();
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_ADC1_Init();
	MX_ADC2_Init();
	// MX_IWDG_Init();
	MX_USB_DEVICE_Init();
	while (*(volatile uint8_t*)&hUsbDeviceFS.dev_state != USBD_STATE_CONFIGURED) {
		continue;
	}
	HAL_Delay(100);
	llogln("%ld STARTED", (unsigned long)HAL_GetTick());
}

static inline
void adc_setup(void) {
	llogln("adc_setup");
	HAL_StatusTypeDef status = 0;
	status = HAL_ADCEx_Calibration_Start(&hadc1);
	if (status != 0) lemergln("%d", status);
	status = HAL_ADCEx_Calibration_Start(&hadc2);
	if (status != 0) lemergln("%d", status);
}

static inline
void adc_start() {
	llogln("");
	HAL_StatusTypeDef status = 0;
	status = HAL_ADC_Start(&hadc2);
	if (status != 0) lemergln("%d", status);
	if (m.count == 0) {
		m.count = ARRAY_SIZE(m.adcdma);
	}
	llogln("Starting ADC conversion with %u samples\n", m.count);
	assert(m.count <= ARRAY_SIZE(m.adcdma));
	status = HAL_ADCEx_MultiModeStart_DMA(&hadc1, m.adcdma, m.count);
	if (status != 0) lemergln("%d", status);
}

static inline
void adc_stop(void) {
	llogln("");
	HAL_StatusTypeDef status = 0;
	status = HAL_ADC_Stop(&hadc1);
	if (status != 0) lemergln("%d", status);
	status = HAL_ADC_Stop(&hadc2);
	if (status != 0) lemergln("%d", status);
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *AdcHandle)
{
	uint64_t sum1 = 0;
	uint64_t sum2 = 0;
	for (uint32_t *i = m.adcdma, * const end = &m.adcdma[m.count];
			i < end;
			++i) {
		const uint32_t val = *i;
		sum1 += (val >> 16) & 0xffff;
		sum2 += (val >>  0) & 0xffff;
	}
	m.sum1 = sum1;
	m.sum2 = sum2;
	m.sumready = true;
}

void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *AdcHandle) {
	asm("bkpt #100");
}

/* ------------------------------------------------------------ */

int help(char *line) {
	printf(
			"Welcome to command line\n"
			"Available commands:\n"
			"\thelp - print this help end exit\n"
			"\tstart <num> - start adc conversion with\n"
			"\t              specified number of conversions\n"
			"\n"
			"Written by Kamil Cukrowski\n"
			"\n"
	);
	return 0;
}

int start(char *line) {
	const char * const first_whitespace = strpbrk(line, " \t");
	if (first_whitespace == NULL) {
		printf("Argument missing - starting with default conversions.");
		m.count = 0;
		return 1;
	}

	const char * const last_whitespace =
			first_whitespace + strspn(first_whitespace, " \t");

	const char * const nptr = last_whitespace;
	char *endptr = NULL;
	errno = 0;
    const long number = strtol(nptr, &endptr, 0);

    /* test return to number and errno values */
    if (nptr == endptr) {
        printf (" number : %lu  invalid  (no digits found, 0 returned)\n", number);
        return -1;
    } else if (errno == ERANGE && number == LONG_MIN) {
        printf (" number : %lu  invalid  (underflow occurred)\n", number);
        return -1;
    } else if (errno == ERANGE && number == LONG_MAX) {
        printf (" number : %lu  invalid  (overflow occurred)\n", number);
        return -1;
    } else if (errno == EINVAL) {  /* not in all c99 implementations - gcc OK */
        printf (" number : %lu  invalid  (base contains unsupported value)\n", number);
    	return -1;
	} else if (errno != 0 && number == 0) {
        printf (" number : %lu  invalid  (unspecified error occurred)\n", number);
    	return -1;
    } else if (errno == 0 && nptr && *endptr != 0) {
        printf (" number : %lu    valid  (but additional characters remain)\n", number);
        return -1;
    } else if ((unsigned long long)number > (unsigned long long)ARRAY_SIZE(m.adcdma)) {
    	printf("number : %lu too buf, max is %d\n", number, (int)ARRAY_SIZE(m.adcdma));
    	return -1;
    } else if (number <= 0) {
    	printf("Number must be greater then 0");
    	return -1;
    }

    printf("Starting with %ld number of conversions\n", number);
    m.count = number;
    return 1;
}

struct commands_s {
	const char *str;
	int (*pnt)(char *line);
} commands[] = {
		{ "help", help, },
		{ "start", start, },
};

static inline
size_t myreadline(char *line, size_t linesize) {
	bool success = false;
	size_t len = 0;

	while (!success) {
		printf("> ");
		fflush(stdout);

		len = 0;

		while (1) {
			const int c = getchar();
			assert(c != EOF);

			if (c == '\n') {
				printf("%c", c);
				success = true;
				break;
			}

			if (!isprint(c)) {
				printf("\nshell: Not printable character detected - %d!\n", c);
				break;
			}

			printf("%c", c);
			fflush(stdout);

			if (len == linesize - 1) {
				printf("\nshell: line too long!\n");
				break;
			}

			line[len++] = c;
		}
	}

	assert(len < linesize);
	line[len] = '\0';

	return len;
}

static inline
void run_command_line(void) {
	stdin_ioctl_set_blocking(true);

	char * const line = (char*)m.adcdma;
	const size_t linesize = sizeof(m.adcdma);

	help(NULL);

	while (1) {
		myreadline(line, linesize);

		const size_t commandlen = strcspn(line, " \t\n");
		if (commandlen == 0) {
			continue;
		}

		int (*f)(char *line) = NULL;
		for (size_t i = 0; i < ARRAY_SIZE(commands); ++i) {
			if (memcmp(commands[i].str, line, commandlen) == 0) {
				f = commands[i].pnt;
				break;
			}
		}

		if (f == NULL) {
			printf("shell: Command not found.\n");
			continue;
		} else {
			const int ret = f(line);
			if (ret < 0) {
				printf("shell: Last command returned: %d\n", ret);
			} else if (ret > 0) {
				printf("shell: Breaking up with command line and starting conversion\n");
				HAL_Delay(1000);
				break;
			}
		}
	}

	stdin_ioctl_set_blocking(false);

	// eat everything from input buffer
	for (int c = EOF; c != EOF; c = getchar()) {
		continue;
	}
}


/* ------------------------------------------------------------ */

static char stdinbuffer[8] = {0};
static char stdoutbuffer[128] = {0};
int main() {
	setvbuf(stdin, stdinbuffer, _IOLBF, sizeof(stdinbuffer));
	setvbuf(stdout, stdoutbuffer, _IOLBF, sizeof(stdoutbuffer));
	setvbuf(stderr, NULL, _IONBF, 0);

	init();
	adc_setup();
	fflush(stdout);
	llogln("%ld", (unsigned long)HAL_GetTick());
	HAL_Delay(2000);

	adc_start();
	while (wdg_refresh(), 1) {

		const uint32_t start = HAL_GetTick();
		while (m.sumready == false) {
			continue;
		}
		__disable_irq();
		const uint64_t sum1 = m.sum1;
		const uint64_t sum2 = m.sum2;
		m.sumready = false;
		__enable_irq();
		const uint32_t stop = HAL_GetTick();
		const uint32_t diff = stop - start;

		const uint32_t tick = HAL_GetTick();
		const double scale = 3.3 / 4096;
		const double factor = scale / m.count;
		const double result1 = (double)sum1 * factor;
		const double result2 = (double)sum2 * factor;
		printf("%10lu %23.17g %23.17g %23.17g %ld\n",
				(unsigned long)tick,
				result1,
				result2,
				result1 - result2,
				(unsigned long)diff);

		const int c = getchar();
		if (c != EOF) {
			ungetc(c, stdin);
			adc_stop();
			run_command_line();
			HAL_Delay(1000);
			adc_start();
		}
	}
}
