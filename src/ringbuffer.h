/*
 * ringbuffer.h
 *
 *  Created on: 28.01.2018
 *      Author: kamil
 */

#ifndef RIRC_RING_BUFFER_H
#define RIRC_RING_BUFFER_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <assert.h>

/* Private Macros ----------------------------------------------------------- */

#define RB_ISPOWEROF2(x)  (!( ((x < 2) || (x & (x - 1))) ))

/* Exported Types ------------------------------------------------------------ */

typedef struct {
	char *data;
    size_t size;

    size_t head;
    size_t fill;
} RingBuffer_t;

/* Exported Functions --------------------------------------------------------- */

#define RB_INIT(_data, _size)   { .data = (_data), .size = (sizeof(char[RB_ISPOWEROF2(_size) ? 1 : -1]) * 0 + (_size)) }
#define RB_INIT_ONSTACK(_size)  RB_INIT( ((char[(_size)]){0}) , (_size) )

RingBuffer_t *rb_new(size_t size);
void rb_free(RingBuffer_t **this);

void rb_write(RingBuffer_t *this, const char from[restrict], size_t bytes);
char *rb_write_pointer(RingBuffer_t *this, size_t *writable);
void rb_write_memcpy(RingBuffer_t *this, const char from[restrict], size_t bytes);
void rb_write_commit(RingBuffer_t *this, size_t bytes);

void rb_read(RingBuffer_t *this, char to[restrict], size_t bytes);
const char *rb_read_pointer(RingBuffer_t *this, size_t offset, size_t *readable);
void rb_read_memcpy(RingBuffer_t *this, char to[restrict], size_t bytes);
void rb_read_commit(RingBuffer_t *this, size_t bytes);

void rb_stream(RingBuffer_t *from, RingBuffer_t *to, size_t bytes);

void rb_flush_safe(RingBuffer_t *this);

static inline void rb_flush(RingBuffer_t *this)
{
	this->fill = 0;
}

static inline bool rb_is_empty(RingBuffer_t *this)
{
    return this->fill == 0;
}

static inline bool rb_is_full(RingBuffer_t *this)
{
    return this->fill == this->size;
}

static inline size_t rb_size(RingBuffer_t *this)
{
    return this->size;
}

static inline size_t rb_used(RingBuffer_t *this)
{
    return this->fill;
}

static inline size_t rb_remain(RingBuffer_t *this)
{
    return this->size - this->fill;
}

static inline void rb_empty(RingBuffer_t *this)
{
    this->head = this->fill = 0;
}

int rb_UnitTest(void);

#endif

