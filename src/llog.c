/*
 * log.c
 *
 *  Created on: 12 wrz 2018
 *      Author: Kamil Cukrowski
 *     License: All rights reserved. Copyright by Netemera(C).
 */
#include <llog.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>

static bool llog_prefix_written = false;

static enum llog_level_e llog_ignore_below = LLOG_DEBUG;

static const char *llog_prioritynames[] = {
		[LLOG_EMERG]    = "EMERG",
		[LLOG_ALERT]    = "ALERT",
		[LLOG_CRIT]     = "CRITI",
		[LLOG_ERR]      = "ERROR",
		[LLOG_WARNING]  = "WARN",
		[LLOG_NOTICE]   = "NOTE",
		[LLOG_INFO]     = "INFO",
		[LLOG_DEBUG]    = "DBG",
};

__attribute__((__pure__))
const char *_llog_get_priority_name(size_t level) {
	assert(level < sizeof(llog_prioritynames)/sizeof(llog_prioritynames[0]));
	return llog_prioritynames[level];
}

__attribute__((__const__))
enum llog_level_e _llog_flags_get_level(int flags) {
	enum { LLOG_LEVEL_MASK = 0x7, };

	static_assert((LLOG_EMERG & LLOG_LEVEL_MASK) == LLOG_EMERG, "");
	static_assert((LLOG_ALERT & LLOG_LEVEL_MASK) == LLOG_ALERT, "");
	static_assert((LLOG_CRIT & LLOG_LEVEL_MASK) == LLOG_CRIT, "");
	static_assert((LLOG_ERR & LLOG_LEVEL_MASK) == LLOG_ERR, "");
	static_assert((LLOG_WARNING & LLOG_LEVEL_MASK) == LLOG_WARNING, "");
	static_assert((LLOG_NOTICE & LLOG_LEVEL_MASK) == LLOG_NOTICE, "");
	static_assert((LLOG_INFO & LLOG_LEVEL_MASK) == LLOG_INFO, "");
	static_assert((LLOG_DEBUG & LLOG_LEVEL_MASK) == LLOG_DEBUG, "");

	const enum llog_level_e ret = flags & LLOG_LEVEL_MASK;

	switch (ret) {
	case LLOG_EMERG:
	case LLOG_ALERT:
	case LLOG_CRIT:
	case LLOG_ERR:
	case LLOG_WARNING:
	case LLOG_NOTICE:
	case LLOG_INFO:
	case LLOG_DEBUG:
		break;
	default:
		assert(!"flags invalid");
		return LLOG_ERR;
	}

	return ret;
}

__attribute__((__weak__))
int _llog_callback(int flags,
		const char file[], int line, const char func[],
		const char fmt[], va_list va);

void _vllog(int flags,
		const char file[], int line, const char func[],
		const char fmt[], va_list ap)
{
	if (_llog_callback) {
		va_list ap2;
		va_copy(ap2, ap);
		const int callback_ret = _llog_callback(flags, file, line, func, fmt, ap2);
		va_end(ap2);
		if (callback_ret) {
			return;
		}
	}

	const enum llog_level_e level = _llog_flags_get_level(flags);
	if (level > llog_ignore_below) {
		return;
	}

	FILE * const out = level >= LLOG_WARNING ? stderr : stdout;

	const bool newline_flag = _llog_flags_get_newline(flags);
	if (newline_flag || !llog_prefix_written) {
		llog_prefix_written = !newline_flag;

		const char * const prioritystr = _llog_get_priority_name(level);
		// fprintf(out, "%s:%d: ", strrchr(file, '/') + 1, line);
		const char * const tmp = strrchr(file, '/');
		const char * const filename = tmp == NULL ? file : (tmp + 1);
		fprintf(out, "%s: %s:%d:%s: ", prioritystr, filename, line, func);

	}
	if (!newline_flag) {
		if (strchr(fmt, '\n') != NULL) {
			llog_prefix_written = false;
		}
	}

	vfprintf(out, fmt, ap);
}

void _llog(int flags,
		const char file[], int line, const char func[],
		const char fmt[], ...) {
	va_list ap;
	va_start(ap, fmt);
	_vllog(flags, file, line, func, fmt, ap);
	va_end(ap);
}

