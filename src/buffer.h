/*
 * buffer.h
 *
 *  Created on: 13 lis 2018
 *      Author: Kamil Cukrowski
 *     License: All rights reserved. Copyright by Netemera(C).
 */

#ifndef BUFFER_H_
#define BUFFER_H_

#include <endian2.h> // htoleXX htobeXX

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdarg.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>


/**
 * Symbolizes a statically allocated buffor,
 * where we *put* or stack things together.
 *
 * The members are not pointers, because:
 * - normally in C language we use size_t (memcpy, memset, etc. all use size_t)
 * - `BUFFER_INIT_ON_STACK` would not work, if it were.
 *
 * Notes on overflow detection:
 * To detect overflow, just allocate one byte bigger buffer then needed.
 * `buffer_push` function pushes at most size passed to it bytes.
 * It will push to fill the buffer at max, if there is no place.
 * So you can detect overflow with `buffer_free > 0`
 * Adding a simple `bool is_overflowed` member to the below structure,
 * will increase it's size by `_Alingof(char*)` which I very much don't like.
 */
struct buffer_s {
	/**
	 * Points to our memory we use store things
	 */
	char * const buf;
	/**
	 * How much free bytes are in buf?
	 */
	size_t used;
	/**
	 * Size of valid bytes pointer to by .buf
	 */
	const size_t size;
};

typedef struct buffer_s buffer_t;

#define BUFFER_INIT2(_buf, _used, _size)  { .buf = (_buf), .used = (_used), .size = (_size) }
#define BUFFER_INIT(_buf, _size)          { .buf = (_buf), .used = 0, .size = (_size) }
#define BUFFER_INIT_ON_STACK(_size)       { .buf = (char[(_size)]){0}, .used = 0, .size = (_size) }

__attribute__((__nonnull__))
static inline
void buffer_init(struct buffer_s *t, char *buf, size_t bufsize) {
	struct buffer_s new = BUFFER_INIT(buf, bufsize);
	memcpy(t, &new, sizeof(*t));
}

__attribute__((__nonnull__))
static inline
void buffer_init2(struct buffer_s *t, char *buf, size_t used, size_t bufsize) {
	struct buffer_s new = BUFFER_INIT2(buf, used, bufsize);
	memcpy(t, &new, sizeof(*t));
}

static inline
void buffer_assert(const struct buffer_s *t) {
	assert(t != NULL);
	assert(t->buf != NULL);
	assert(0 != t->size);
	assert(t->used <= t->size);
}

__attribute__((__nonnull__(1)))
int buffer_push(struct buffer_s *t, const void *src, size_t nbyte);

__attribute__((__nonnull__(1)))
size_t buffer_pop(struct buffer_s *t, void *dest, size_t nbyte);

__attribute__((__nonnull__))
int buffer_vprintf(struct buffer_s *t, const char *fmt, va_list va);

__attribute__((__nonnull__, __format__(__printf__, 2, 3)))
int buffer_printf(struct buffer_s *t, const char *fmt, ...);

__attribute__((__pure__, __nonnull__, __warn_unused_result__))
static inline
size_t buffer_size(const struct buffer_s *t) {
	return t->size;
}

__attribute__((__pure__, __nonnull__, __warn_unused_result__))
static inline
size_t buffer_free(const struct buffer_s *t)
{
	return t->size - t->used;
}

__attribute__((__pure__, __nonnull__, __warn_unused_result__))
static inline
size_t buffer_used(const struct buffer_s *t) {
	return t->used;
}

__attribute__((__pure__, __nonnull__, __warn_unused_result__))
static inline
char *buffer_data(const struct buffer_s *t) {
	return t->buf;
}

__attribute__((__pure__, __nonnull__, __warn_unused_result__))
static inline
char *buffer_begin(const struct buffer_s *t) {
	return &t->buf[0];
}

__attribute__((__pure__, __nonnull__, __warn_unused_result__))
static inline
char *buffer_end(const struct buffer_s *t) {
	return &t->buf[t->used];
}

__attribute__((__pure__, __nonnull__, __warn_unused_result__))
static inline
const char *buffer_cbegin(const struct buffer_s *t) {
	return &t->buf[0];
}

__attribute__((__pure__, __nonnull__, __warn_unused_result__))
static inline
const char *buffer_cend(const struct buffer_s *t) {
	return &t->buf[t->used];
}

__attribute__((__nonnull__))
static inline
void buffer_flush(struct buffer_s *t) {
	t->used = 0;
}

__attribute__((__nonnull__))
static inline
int buffer_set_used(struct buffer_s *t, size_t used) {
	if (used > t->size) {
		return -EINVAL;
	}
	t->used = used;
	buffer_assert(t);
	return 0;
}

__attribute__((__nonnull__))
static inline
int buffer_used_inc(struct buffer_s *t, size_t inc) {
	if (t->used > SIZE_MAX - inc) {
		return -EINVAL;
	}
	if (t->used + inc > t->size) {
		return -EINVAL;
	}
	t->used += inc;
	buffer_assert(t);
	return 0;
}

__attribute__((__nonnull__))
static inline
int buffer_push_c(struct buffer_s *t, char c) {
	return buffer_push(t, &c, sizeof(char));
}

__attribute__((__nonnull__))
static inline
int buffer_push_buffer(struct buffer_s *to, const struct buffer_s *from) {
	return buffer_push(to, buffer_cbegin(from), buffer_used(from));
}

__attribute__((__nonnull__))
static inline
int buffer_strput(struct buffer_s *t, const char *str) {
	return buffer_push(t, str, strlen(str) + 1);
}

__attribute__((__nonnull__))
static inline
int buffer_strnput(struct buffer_s *t, const char *str, size_t max) {
	const size_t str_len = strlen(str);
	if (max > str_len) {
		max = str_len;
	}
	return buffer_push(t, str, max);
}

__attribute__((__nonnull__))
static inline
int buffer_strcat(struct buffer_s *t, const char *str) {
	return buffer_push(t, str, strlen(str));
}

__attribute__((__nonnull__))
static inline
int buffer_push_u8(struct buffer_s *t, uint8_t val) {
	return buffer_push(t, &val, sizeof(val));
}

__attribute__((__nonnull__))
static inline
int buffer_push_ube8(struct buffer_s *t, uint8_t val) {
	return buffer_push(t, &val, sizeof(val));
}

__attribute__((__nonnull__))
static inline
int buffer_push_ule8(struct buffer_s *t, uint8_t val) {
	return buffer_push(t, &val, sizeof(val));
}

__attribute__((__nonnull__))
static inline
int buffer_push_u16(struct buffer_s *t, uint16_t val) {
	return buffer_push(t, &val, sizeof(val));
}

__attribute__((__nonnull__))
static inline
int buffer_push_ube16(struct buffer_s *t, uint16_t val) {
	return buffer_push_u16(t, htobe16(val));
}

__attribute__((__nonnull__))
static inline
int buffer_push_ule16(struct buffer_s *t, uint16_t val) {
	return buffer_push_u16(t, htole16(val));
}

__attribute__((__nonnull__))
static inline
int buffer_push_u32(struct buffer_s *t, uint32_t val) {
	return buffer_push(t, &val, sizeof(val));
}

__attribute__((__nonnull__))
static inline
int buffer_push_ube32(struct buffer_s *t, uint32_t val) {
	return buffer_push_u32(t, htobe32(val));
}

__attribute__((__nonnull__))
static inline
int buffer_push_ule32(struct buffer_s *t, uint32_t val) {
	return buffer_push_u32(t, htole32(val));
}

__attribute__((__nonnull__))
static inline
int buffer_push_u64(struct buffer_s *t, uint64_t val) {
	return buffer_push(t, &val, sizeof(val));
}

__attribute__((__nonnull__))
static inline
int buffer_push_ube64(struct buffer_s *t, uint64_t val) {
	return buffer_push_u64(t, htobe64(val));
}

__attribute__((__nonnull__))
static inline
int buffer_push_ule64(struct buffer_s *t, uint64_t val) {
	return buffer_push_u64(t, htole64(val));
}

__attribute__((__nonnull__))
static inline
int buffer_copy(struct buffer_s *t, const struct buffer_s *from) {
	buffer_flush(t);
	return buffer_push_buffer(t, from);
}

__attribute__((__warn_unused_result__))
int buffer_unittest(void);

#endif /* BUFFER_H_ */

