/*
 * minmax.h
 *
 *  Created on: 31.01.2018
 *      Author: kamil
 */

#ifndef SRC_SYSTEM_MINMAX_H_
#define SRC_SYSTEM_MINMAX_H_

/*!
 * \brief Returns the minimum value between a and b
 *
 * \param [IN] a 1st value
 * \param [IN] b 2nd value
 * \retval minValue Minimum value
 */
#ifndef MIN
#ifdef __GNUC__
#define MIN(a,b) __extension__({ \
		__typeof__ (a) _a = (a); \
		__typeof__ (b) _b = (b); \
		_a < _b ? _a : _b; })
#else
#define MIN(a,b) ((a)<(b)?(a):(b))
#endif
#endif

/*!
 * \brief Returns the maximum value between a and b
 *
 * \param [IN] a 1st value
 * \param [IN] b 2nd value
 * \retval maxValue Maximum value
 */
#ifndef MAX
#ifdef __GNUC__
#define MAX(a,b) __extension__({ \
		__typeof__ (a) _a = (a); \
		__typeof__ (b) _b = (b); \
		_a > _b ? _a : _b; })
#else
#define MAX(a,b) ((a)>(b)?(a):(b))
#endif
#endif

#define MINMAX(a,min,max) ( MIN(MAX(a,min),max) )

#endif /* SRC_SYSTEM_MINMAX_H_ */
