/*
 * endian.h
 *
 *  Created on: 01.03.2018
 *      Author: kamil
 */

#ifndef LIBC_ENDIAN2_H_
#define LIBC_ENDIAN2_H_

#ifndef __BYTE_ORDER__
#error __BYTE_ORDER__ is not defined by your compiler
#endif
#ifndef __ORDER_LITTLE_ENDIAN__
#error __ORDER_LITTLE_ENDIAN__ is not defined by your compiler
#endif

#ifndef __bswap16
#define	__bswap16(_x)	__builtin_bswap16(_x)
#endif

#ifndef __bswap32
#define	__bswap32(_x)	__builtin_bswap32(_x)
#endif

#ifndef __bswap64
#define	__bswap64(_x)	__builtin_bswap64(_x)
#endif

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#  define htobe16(x) __bswap16 (x)
#  define htole16(x) (x)
#  define be16toh(x) __bswap16 (x)
#  define le16toh(x) (x)

#  define htobe32(x) __bswap32 (x)
#  define htole32(x) (x)
#  define be32toh(x) __bswap32 (x)
#  define le32toh(x) (x)

#  define htobe64(x) __bswap64 (x)
#  define htole64(x) (x)
#  define be64toh(x) __bswap64 (x)
#  define le64toh(x) (x)
#else
#  define htobe16(x) (x)
#  define htole16(x) __bswap16 (x)
#  define be16toh(x) (x)
#  define le16toh(x) __bswap16 (x)

#  define htobe32(x) (x)
#  define htole32(x) __bswap32 (x)
#  define be32toh(x) (x)
#  define le32toh(x) __bswap32 (x)

#  define htobe64(x) (x)
#  define htole64(x) __bswap64 (x)
#  define be64toh(x) (x)
#  define le64toh(x) __bswap64 (x)
#endif

#endif /* LIBC_ENDIAN2_H_ */

