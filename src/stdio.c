/*
 * stdio.c
 *
 *  Created on: 1 lis 2019
 *      Author: kamil
 */
#include <reent.h>
#include <stdio.h>
#include <assert.h>
#include <usbd_cdc_if.h>
#include <stdint.h>
#include <array_size.h>
#include <ringbuffer.h>

void wdg_refresh(void);

static RingBuffer_t recv = RB_INIT_ONSTACK(128);

void CDC_Receive_FS_callback(uint8_t *buf, uint32_t len) {
	const size_t remain = rb_remain(&recv);
	const size_t tocopy = remain > len ? len : remain;
	rb_write(&recv, (char*)buf, tocopy);
}

_ssize_t _write_r(struct _reent *r, int fd, const void *buf, size_t nbyte) {
	assert(fd == 1 || fd == 2);
	const uint8_t ret = CDC_Transmit_FS((uint8_t*)buf, nbyte);
	wdg_refresh();
	if (ret != 0) {
		return -1;
	}
	return nbyte;
}

static bool read_blocking = false;

void stdin_ioctl_set_blocking(bool state) {
	read_blocking = state;
}

_ssize_t _read_r(struct _reent *r, int fd, void *buf, size_t nbyte) {
	assert(fd == 0);
	do {
		const size_t used = rb_used(&recv);
		const size_t tocopy = nbyte > used ? used : nbyte;
		if (tocopy) {
			rb_read(&recv, buf, tocopy);
			return tocopy;
		}
		wdg_refresh();
	} while (read_blocking);
	return -1;
}
