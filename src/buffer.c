/*
 * buffer.c
 *
 *  Created on: 13 lis 2018
 *      Author: Kamil Cukrowski
 *     License: All rights reserved. Copyright by Netemera(C).
 */
#include "buffer.h"

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#define BUFFER_OVERFLOW()   assert(!!"buffer overflow")
#define BUFFER_UNDERFLOW()  assert(!!"buffer underflow")

/**
 * Pushes data back to the buffer
 * @param t the buffer object
 * @param src pointer to bytes to copy
 * @param nbyte size of the pointer
 * @return 0 on success
 *         -ENOMEM when no available memory left
 */
int buffer_push(struct buffer_s *t, const void *src, size_t nbyte)
{
	buffer_assert(t);
	int ret = 0;

	const size_t free = buffer_free(t);
	if (nbyte > free) {
		nbyte = free;
		BUFFER_OVERFLOW();
		ret = -ENOMEM;
	}
	if (src) {
		memcpy(buffer_end(t), src, nbyte);
	}
	t->used += nbyte;

	return ret;
}

/**
 * This is pop front!
 * Attempts to read nbyte bytes from the buffer object and place it at dest.
 * @param t the buffer object
 * @param dest the
 * @param nbyte
 * @return the number of bytes pop-ed
 */
size_t buffer_pop(struct buffer_s *t, void *dest, size_t nbyte)
{
	buffer_assert(t);
	const size_t used = buffer_used(t);
	if (nbyte > used) {
		nbyte = used;
		BUFFER_UNDERFLOW();
	}
	if (dest) {
		memcpy(dest, buffer_begin(t), nbyte);
	}
	memmove(buffer_begin(t), buffer_begin(t) + nbyte, used - nbyte);
	t->used -= nbyte;

	return nbyte;
}

int buffer_vprintf(struct buffer_s *t, const char *fmt, va_list va)
{
	buffer_assert(t);
	const size_t free = buffer_free(t);
	int ret = vsnprintf(buffer_end(t), free, fmt, va);
	if (ret > (int)free) {
		ret = (int)free;
	}
	buffer_used_inc(t, ret);
	return ret;
}

int buffer_printf(struct buffer_s *t, const char *fmt, ...) {
	va_list va;
	va_start(va, fmt);
	const int ret = buffer_vprintf(t, fmt, va);
	va_end(va);
	return ret;
}

#define TEST(expr)  do{ if(!(expr)) { return -__LINE__; } }while(0)

int buffer_unittest(void)
{
	char data[256] = {0};
	for (int i = 0; i < 255; ++i) {
		int j = i + '0';
		while (j > 'Z') {
			j -= 'Z';
		}
		data[i] = j;
	}

	{
		struct buffer_s b = BUFFER_INIT_ON_STACK(256);
		TEST(buffer_free(&b) == 256);
		TEST(buffer_used(&b) == 0);
		TEST(buffer_size(&b) == 256);
		TEST(buffer_push(&b, data, 100) == 0);
		TEST(buffer_used(&b) == 100);
		TEST(buffer_free(&b) == 256 - 100);
		TEST(buffer_size(&b) == 256);
		TEST(memcmp(buffer_begin(&b), data, buffer_used(&b)) == 0);
	}
	{
		struct buffer_s b = BUFFER_INIT_ON_STACK(256);
		char buf[256];

		TEST(buffer_push(&b, data, 256) == 0);
		TEST(buffer_used(&b) == 256);
		TEST(buffer_free(&b) == 0);
		TEST(buffer_size(&b) == 256);
		TEST(memcmp(buffer_begin(&b), data, buffer_used(&b)) == 0);

		TEST(buffer_push(&b, data, 1) == -ENOMEM);

		char c = 0;
		TEST(buffer_pop(&b, &c, 1) == 1);
		TEST(c == '0');
		TEST(buffer_used(&b) == 255);
		TEST(buffer_free(&b) == 1);
		TEST(buffer_size(&b) == 256);

		TEST(buffer_pop(&b, &c, 1) == 1);
		TEST(c == '1');
		TEST(buffer_used(&b) == 254);
		TEST(buffer_free(&b) == 2);
		TEST(buffer_size(&b) == 256);

		TEST(buffer_pop(&b, &buf, 254) == 254);
		TEST(buffer_used(&b) == 0);
		TEST(buffer_free(&b) == 256);
		TEST(buffer_size(&b) == 256);
		TEST(memcmp(buf, &data[2], 254) == 0);

		TEST(buffer_pop(&b, NULL, 256) == 0);
		TEST(buffer_used(&b) == 0);
		TEST(buffer_free(&b) == 256);
		TEST(buffer_size(&b) == 256);
	}
	{
		struct buffer_s b = BUFFER_INIT_ON_STACK(256);
		TEST(buffer_push(&b, data, 256) == 0);
		struct buffer_s b2;
		buffer_init(&b2, (char[256]){0}, 256);
		buffer_push_buffer(&b2, &b);
		TEST(memcmp(buffer_begin(&b2), data, buffer_used(&b2)) == 0);
	}
	return 0;
}

