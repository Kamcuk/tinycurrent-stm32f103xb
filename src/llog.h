/*
 * llog.h
 *
 *  Created on: 22.06.2017
 *      Author: Kamil Cukrowski
 */
#pragma once

#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

enum llog_flags_e {
	LLOG_NEWLINE = 0x80,
};

enum llog_level_e {
	LLOG_EMERG    = 0,
	LLOG_ALERT    = 1,
	LLOG_CRIT     = 2,
	LLOG_ERR      = 3,
	LLOG_WARNING  = 4,
	LLOG_NOTICE   = 5,
	LLOG_INFO     = 6,
	LLOG_DEBUG    = 7,
};

__attribute__((__const__))
enum llog_level_e _llog_flags_get_level(int flags);

__attribute__((__const__))
static inline
bool _llog_flags_get_newline(int flags) {
	return flags & LLOG_NEWLINE;
}

/**
 * Weak function callback called on _llog_generic.
 * If returns nonzero, llog function returns.
 */
__attribute__((__nonnull__))
int _llog_callback(int flags,
		const char file[], int line, const char func[],
		const char fmt[], va_list va);

__attribute__((__nonnull__))
void _vllog(int flags,
		const char file[], int line, const char func[],
		const char fmt[], va_list ap);

__attribute__((__nonnull__))
__attribute__((__format__(__printf__, 5, 6)))
void _llog(int flags,
		const char file[], int line, const char func[],
		const char fmt[], ...);

#if __GNUC__ && __cplusplus
#define LLOG_FUNC  __PRETTY_FUNCTION__
#else
#define LLOG_FUNC  __func__
#endif

#ifndef LLOG_NAME
#define LLOG_NAME ""
#endif

#define lflogln(flags, fmt, ...)  lflog((flags) | LLOG_NEWLINE, (LLOG_NAME fmt "\n"), ##__VA_ARGS__)
#define lflog(flags, fmt, ...)    _llog((flags), __FILE__, __LINE__, LLOG_FUNC, (fmt), ##__VA_ARGS__)

#define lemergln(fmt, ...)         lflogln(LLOG_EMERG,   fmt, ##__VA_ARGS__)
#define lemerg(fmt, ...)           lflog(  LLOG_EMERG,   fmt, ##__VA_ARGS__)
#define lalertln(fmt, ...)         lflogln(LLOG_ALERT,   fmt, ##__VA_ARGS__)
#define lalert(fmt, ...)           lflog(  LLOG_ALERT,   fmt, ##__VA_ARGS__)
#define lcritln(fmt, ...)          lflogln(LLOG_CRIT,    fmt, ##__VA_ARGS__)
#define lcrit(fmt, ...)            lflog(  LLOG_CRIT,    fmt, ##__VA_ARGS__)
#define lerrln(fmt, ...)           lflogln(LLOG_ERR,     fmt, ##__VA_ARGS__)
#define lerr(fmt, ...)             lflog(  LLOG_ERR,     fmt, ##__VA_ARGS__)
#define lwarnln(fmt, ...)          lflogln(LLOG_WARNING, fmt, ##__VA_ARGS__)
#define lwarn(fmt, ...)            lflog(  LLOG_WARNING, fmt, ##__VA_ARGS__)
#define lnoticeln(fmt, ...)        lflogln(LLOG_NOTICE,  fmt, ##__VA_ARGS__)
#define lnotice(fmt, ...)          lflog(  LLOG_NOTICE,  fmt, ##__VA_ARGS__)

#if LLOG_DISABLE_INFO
#define linfoln(fmt, ...)          ((void)0)
#define linfo(fmt, ...)            ((void)0)
#else
#define linfoln(fmt, ...)          lflogln(LLOG_INFO,    fmt, ##__VA_ARGS__)
#define linfo(fmt, ...)            lflog(  LLOG_INFO,    fmt, ##__VA_ARGS__)
#endif

#define llogln(fmt, ...)           linfoln(fmt, ##__VA_ARGS__)
#define llog(fmt, ...)             linfo(fmt, ##__VA_ARGS__)

#if LLOG_DISABLE_DEBUG
#define ldebugln(fmt, ...)         ((void)0)
#define ldebug(fmt, ...)           ((void)0)
#else
#define ldebugln(fmt, ...)         lflogln(LLOG_DEBUG,   fmt, ##__VA_ARGS__)
#define ldebug(fmt, ...)           lflog(  LLOG_DEBUG,   fmt, ##__VA_ARGS__)
#endif



